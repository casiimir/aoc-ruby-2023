file = File.open("./input.txt")
string = file.read.to_s
file.close

def extract_possible_g(game, regex, max)
  colors = game.scan(regex).flatten.map(&:to_i)
  colors.all? { |v| v <= max }
end

part1 = string.split("\n").reduce(0) do |sum, game|
  red_possibile_g = extract_possible_g(game, /(\d+)\sred/, 12)
  green_possible_g = extract_possible_g(game, /(\d+)\sgreen/, 13)
  blue_possible_g = extract_possible_g(game, /(\d+)\sblue/, 14)

  if red_possibile_g && green_possible_g && blue_possible_g
    sum + game[/\bGame (\d+)/, 1].to_i
  else
    sum
  end
end

part2 = string.split("\n").reduce(0) do |sum, game|
  red_max = game.scan(/(\d+)\sred/).flatten.map(&:to_i).max()
  green_max = game.scan(/(\d+)\sgreen/).flatten.map(&:to_i).max()
  blue_max = game.scan(/(\d+)\sblue/).flatten.map(&:to_i).max()
  sum += red_max * green_max * blue_max
end
