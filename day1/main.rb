file = File.open("./input.txt")
string = file.read.to_s
file.close

p string.split("\n").reduce(0) do |sum, str|
  row = str.split('').select { |c| c.to_i > 0 }
  sum + (row[0] + row[-1]).to_i
end
